class NewsController < ApplicationController
    require 'rest-client'

    def init_api
        # Init
        key = "a257760d8a234453b48d69ebe431368e"
        ip = request.env["HTTP_X_FORWARDED_FOR"]
        puts "#{ip}"
        
        begin
            resp = RestClient.get("http://www.geoplugin.net/json.gp?ip=#{ip}")
            json_ip = JSON.parse resp.body
            country_code = json_ip["geoplugin_countryCode"]
            #response = RestClient.get("https://newsapi.org/v2/top-headlines?country=#{country_code}&apiKey=#{key}")
            domains = "larepublica.co,clarin.com,pyme.emol.com,nacion.com,eleconomista.com.mx,elcomercio.pe,uruguayxxi.gub.uy,prensalibre.com,es.euronews.com,economist.com,wsj.com,forbes.com,fortune.com,asean.org,cincodias.elpais.com"
            response = RestClient.get("https://newsapi.org/v2/everything?q=(empresas OR business)&domains=#{domains}&pageSize=20&apiKey=#{key}")
          rescue RestClient::Exception => err
            puts 'Server Error'
            render json:{ error: err}
          rescue SocketError => err
            puts 'Socket Error'
            render json:{ error: err}
          else
            json = JSON.parse response.body
            @news = json['articles']
            #render json: json_ip     
          end
      
    end

    def index
        init_api
    end
end
